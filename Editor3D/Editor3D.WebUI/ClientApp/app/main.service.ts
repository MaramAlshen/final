import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, RequestMethod } from '@angular/http';

const httpOptions = {
  headers: new Headers({ 'Content-Type': 'application/json' })
};

//@Injectable({
  //providedIn: 'root'
//})

@Injectable()

export class MainService {
  private vendorName = new BehaviorSubject<string>('');
  currentVendor = this.vendorName.asObservable();

    private objName = new BehaviorSubject<Object>({ 'anatomicName' : "1", 'vendorName' : "2", 'action' : "3", 'time' : "4"});
  currentObj = this.objName.asObservable()

  private arrName = new BehaviorSubject<Array<Object>>([]);
  currentArr = this.arrName.asObservable()

    //constructor(private HttpClient:Http) { }
    constructor(private http: Http, private httpClient: HttpClient) { }

  addVendor(vendorFileName: string) {
      this.vendorName.next(vendorFileName);
  }

  generateNote(anatomicName:string,vendorName:string,action:string) {
    let obj:Object={
      action:action,
      vendorName:vendorName,
      anatomicName:anatomicName,
      time:Date()
    }
    this.objName.next(obj);
  }

    sendArray(arr: Object[]) {
      console.log("PPPPPPPPPPPPPPPPP")
    this.arrName.next(arr);
    console.log("0000000000000000000", this.arrName.value)
    this.postAllNotes(this.arrName.value[0])
  }

  getNotes() : Object[] {
      return JSON.parse(<any>this.http.get('http://localhost:65155/api/gnote'));
  }

    postAllNotes(data: {}) {
        console.log("jhfkdhkfjsa")
        var d = {"Action": "A 2","Anatomic": "Ana 2","Vendor": "v 2"}
        var body = JSON.stringify(d);
        console.log("ppppppppppppppppp", body)
        var headerOptions = new Headers({ 'Content-Type': 'application/json' });
        var requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post('http://localhost:65155/home/api/GNote', body, requestOptions).map(x => x);
    }

  //  postAllNotes(data: any) {
  //    console.log("................", data)
  //    return this.http.post('http://localhost:65155/api/gnote',data, requestOptions);
  //}

}

