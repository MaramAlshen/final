import { Component, Inject, OnInit } from '@angular/core';
//import { Component, OnInit } from '@angular/core';

import { MainService } from "../../main.service";
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { post } from 'selenium-webdriver/http';

@Component({
    selector: 'fetchdata',
    templateUrl: './fetchdata.component.html',
    styleUrls: ['./fetchdata.component.css']
})

export class FetchDataComponent {
    
    //constructor() {
        
    //}
//}

//@Component({
//    selector: 'app-gnote',
//    templateUrl: './gnote.component.html',
//    styleUrls: ['./gnote.component.css']
//})

//export class GNoteComponent implements OnInit {

    private allNotes: Object[] = [];
    public notes : any;

    constructor(private _demoService: MainService, private data: MainService) { }

    ngOnInit() {
        // this.allNotes = this.getNotes();
        // this.getNotes();
        this.data.currentObj.subscribe(
            (object : any) => {
                this.allNotes.push(object)
            }
        )
        console.log(this.allNotes, ' here all ');
    }

    public noteInfo(object : any): void { }

    // public getNotes() : void {    
    //   this._demoService.getNotes().subscribe(
    //     // the first argument is a function which runs on success
    //     data => { this.notes = data;console.log('data ',data)},
    //     // the second argument is a function which runs on error
    //     err => console.error(err),
    //     // the third argument is a function which runs on completion
    //     () => console.log('done loading notes')
    //   );
    // }

    public postAllNotes(): void {
        this._demoService.postAllNotes(this.allNotes).subscribe(
            // the first argument is a function which runs on success
            (data : any) => { this.notes = data },
            // the second argument is a function which runs on error
            (err : any) => console.error(err),
            // the third argument is a function which runs on completion
            () => console.log('done loading notes')
        );
    }

    public saveAll(): void {
        this.data.sendArray(this.allNotes);
    }

}



