import { Component, OnInit } from '@angular/core';
import { MainService } from "../../main.service";


@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css'],
    providers: [MainService]
})

//@Component({
//    selector: 'app-vendor-list',
//    templateUrl: './vendor-list.component.html',
//    styleUrls: ['./vendor-list.component.css']
//})

export class NavMenuComponent {

// export class VendorListComponent implements OnInit {
    public vendorName: string | undefined;

    constructor(private data: MainService) { }

    public ngOnInit() : void {
        this.data.currentVendor.subscribe(vendor => this.vendorName = vendor)
    }
    public addVendor(): void {
        let vendorFileName: any = (<HTMLInputElement>document.getElementById('selectOption')).value;
        this.data.addVendor(vendorFileName)
    }
}

