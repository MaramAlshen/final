import { Component } from '@angular/core';
import { MainService } from "../../main.service";

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: [MainService]
})
export class AppComponent {
}
