﻿using Editor3D.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Editor3D.Data.Interfaces
{
   public interface IGNote
    {
        void GeneratNote(GNote gNote);
        IEnumerable<GNote> GetAllGNotes();
        GNote GetGNoteById(int gNotId);
    }
}
