﻿using Editor3D.Data.Interfaces;
using Editor3D.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Editor3D.Data.Repositories
{
    public class SceanRepo : IScene
    {
        private readonly AppDbContext _appDbContext;
       
        
        //Constaracter
        public SceanRepo(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public void AddScene(Scene scene)
        {
            _appDbContext.Scenes.Add(scene);
            _appDbContext.SaveChanges();
        }
        
        //...............................................
        //Get Last Scene 
        public IEnumerable<Scene> GetLastScene()
        {
            return _appDbContext.Scenes;
        }
    }
}