﻿using Editor3D.Data.Interfaces;
using Editor3D.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Editor3D.Data.Repositories
{
    public class GNoteRepo : IGNote
    {
        private readonly AppDbContext _appContext;

        //Constracter
        public GNoteRepo(AppDbContext appDbContext)
        {
            _appContext = appDbContext;
        }
        //........................................
        //Add Generate New Note
        public void GeneratNote(GNote gNote)
        {
            _appContext.GNotes.Add(gNote);
            _appContext.SaveChanges();
        }
        //........................................
        //Get All Note
        public IEnumerable<GNote> GetAllGNotes()
        {
            return _appContext.GNotes;
        }
        //........................................
        //Get Note By Id
        public GNote GetGNoteById(int gNotId)
        {
            return _appContext.GNotes.FirstOrDefault(g => g.GNoteId == gNotId);
        }
    }
}